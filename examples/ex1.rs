/*
 * write a programme to add two natural numbers together, and to
 * multiply them. what is a natural number? It is a whole non-
 * negative number.
 *
 * test values to try your code out on:
 *       1 +  3 =  4,  1 *  3 =   3,
 *      11 + 11 = 22, 11 * 11 = 121,
 *      10 +  8 = 18, 10 *  8 =  80
 */
fn main() {
    let n: Vec<u64> = std::env::args()
        .skip(1)
        .map(|arg| u64::from_str_radix(&arg, 10).expect("Invalid input"))
        .collect::<_>();

    if n.len() > 2 {
        println!("Only two numbers accepted.");
    }
    else {
        println!("{} + {} = {}", n[0], n[1], n[0] + n[1]);
        println!("{} * {} = {}", n[0], n[1], n[0] * n[1]);
    }
}
