/*
 * Problem 2 :- Based on your code from problem 1, write another
 * programme that adds a list of numbers of any length together,
 * stopping if it encounters a negative number and delivering the
 * sum so far, not including the negative number or any numbers,
 * or other data, past it.
 *
 * what is a rogue value?
 * It is a value of the same type as the valid data, but outside
 * the range of acceptable values. It is a way of flagging up when
 * the data ends, but without raising an exception or using an
 * aggregate data type for all values.
 *
 * test data:
 *   1,2,3,4,5,6,7,8,9,10,-2 should give the answer 55
 *   2,2,2,2,-100, 5, 6, 7 should give the answer 8
 *
 * the  big test will do over 100 numbers
 */

fn main() {
    let res: u64 = std::env::args()
        .skip(1)
        .map(|arg| u64::from_str_radix(&arg, 10))
        .take_while(|r| r.is_ok())
        .fold(0, |acc, r| acc + r.unwrap());

    println!("{}", res);
}
