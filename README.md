My solutions to excercises posted under the `#code-sprint` tag on Diaspora december 2017.

All code released under the Gnu General Public Licence version 3 (GPLv3).
